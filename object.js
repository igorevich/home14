//--1
let a = {
    b: 22,
    c: 18,
    d: 17,
}

let minKey = "d";
let w = a[minKey];
for (key in a) {
    if (w > a[key]) { 
        w = a[key];
        minKey = key;
    }
}
delete a[minKey]

//--2

const b = {
    ...a, f: {
        h: 'hi',
        j: 123,
       }
    }



//--3

b.f.j = w;

//--4

('d' in a) ? сonsole.log('да') : console.log('нет');


//--5
const aa = {
    a: 5,
}
aa = {...aa, b = 78}; //не догоняю в чем ошибка... тут в консоле выдает



