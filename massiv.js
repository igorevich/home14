const arr = [1, 2, 23, "mama", {a: 1, b: "2"}]

//--2
let arrobj = arr[arr.length-1]
console.log(arrobj)

//--3
const alf = arr[0];
arr[0] = arr[1];
arr[1] = arr[0];
console.log(arr)

//--4
aar = [...arr, ...['ek', 82]];
console.log(aar);


//--5

for (key in arr){
    if (typeof arr[key] === 'number'){
        arr[key] = arr[key] +2;
    }
}
console.log(arr)

//--6

const arrNumbers = [2, 56,23,1,2, 1];

//--7

let min = arrNumbers[0];
let minKey = 0;
for (key in arrNumbers) {
    if (min > arrNumbers[key]) {
        min = arrNumbers[key];
        minKey = key;
    }
}
arrNumbers[minKey] = 99;


